let fs = require("fs");
let callback1 = require("./callback1.cjs");
let callback2 = require("./callback2.cjs");
let callback3 = require("./callback3.cjs");
let boardsValue = require("./boards.json");

function callback6(boardName, pathFile1, pathFile2, pathFile3) {
  setTimeout(() => {
    let idFound = boardsValue.find((eachData) => {
      return eachData.name === boardName;
    });
    callback1(pathFile1, idFound.id, (errorGot1, data1) => {
      if (errorGot1) {
        console.error("Error:");
        console.error(errorGot1);
      } else {
        console.log(data1);
        callback2(pathFile2, data1.id, (errorGot2, data2) => {
          if (errorGot2) {
            console.error("Error:");
            console.error(errorGot2);
          } else {
            console.log(data2);
            let idFound = data2.reduce((allInfo, eachInfo) => {
              allInfo.push(eachInfo.id);
              return allInfo;
            }, []);
            idFound.forEach((eachData) => {
              callback3(pathFile3, eachData, (errorGot3, data3) => {
                if (errorGot3) {
                  console.log("Id:" + eachData);
                  console.error("Error:");
                  console.error("ID NOT FOUND\n");
                } else {
                  console.log("Id:" + eachData);
                  console.log(data3);
                }
                console.log("\n");
              });
            });
          }
        });
      }
    });
  }, 2 * 1000);
}

module.exports = callback6;
