let fs = require("fs");
let callback1 = require("./callback1.cjs");
let callback2 = require("./callback2.cjs");
let callback3 = require("./callback3.cjs");
let boardsValue = require("./boards.json");

function callback4(boardName, cardName, pathFile1, pathFile2, pathFile3) {
  setTimeout(() => {
    let idFound = boardsValue.find((eachData) => {
      return eachData.name === boardName;
    });
    callback1(pathFile1, idFound.id, (errorGot1, data1) => {
      if (errorGot1) {
        console.error("Error:");
        console.error(errorGot1);
      } else {
        console.log(data1);
        callback2(pathFile2, data1.id, (errorGot2, data2) => {
          if (errorGot2) {
            console.error("Error:");
            console.error(errorGot2);
          } else {
            console.log(data2);
            let idFound = data2.find((eachData) => {
              return eachData.name === cardName;
            });
            callback3(pathFile3, idFound.id, (errorGot3, data3) => {
              if (errorGot3) {
                console.error("Error:");
                console.error(errorGot3);
              } else {
                console.log(data3);
              }
            });
          }
        });
      }
    });
  }, 2 * 1000);
}

module.exports = callback4;
