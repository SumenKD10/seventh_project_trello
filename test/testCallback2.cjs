let callback2 = require("../callback2.cjs");

let idToPass = "mcu453ed";
let pathFile = "./lists.json";

callback2(pathFile, idToPass, (errorGot, data) => {
  if (errorGot) {
    console.log("Error:");
    console.log(errorGot);
  } else {
    console.log(data);
  }
});
