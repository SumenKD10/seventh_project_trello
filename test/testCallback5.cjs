let callback5 = require("../callback5.cjs");

let boardName = "Thanos";
let cardName = ["Mind", "Space"];
let pathFile1 = "./boards.json";
let pathFile2 = "./lists.json";
let pathFile3 = "./cards.json";

callback5(boardName, cardName, pathFile1, pathFile2, pathFile3);
