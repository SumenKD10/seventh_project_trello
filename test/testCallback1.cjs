let callback1 = require("../callback1.cjs");

let idToPass = "mcu453ed";
let pathFile = "./boards.json";

callback1(pathFile, idToPass, (errorGot, data) => {
  if (errorGot) {
    console.log("Error:");
    console.log(errorGot);
  } else {
    console.log(data);
  }
});
