let callback3 = require("../callback3.cjs");

let idToPass = "qwsa221";
let pathFile = "./cards.json";

callback3(pathFile, idToPass, (errorGot, data) => {
  if (errorGot) {
    console.log("Error:");
    console.log(errorGot);
  } else {
    console.log(data);
  }
});
