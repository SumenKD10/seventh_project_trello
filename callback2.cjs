let fs = require("fs");

function callback2(pathFile, idPassed, callback1idlistFind) {
  setTimeout(() => {
    fs.readFile(pathFile, "utf8", (errorGot, data) => {
      if (errorGot) {
        console.error("Error:");
        console.error(errorGot);
      } else {
        let dataGot = JSON.parse(data);
        if (dataGot[idPassed] === undefined) {
          let newError = new Error("Error Got Here");
          callback1idlistFind(newError, null);
        } else {
          callback1idlistFind(null, dataGot[idPassed]);
        }
      }
    });
  }, 2 * 1000);
}

module.exports = callback2;
