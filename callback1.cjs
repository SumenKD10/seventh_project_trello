let fs = require("fs");

function callback1(pathFile, idPassed, callback1idPassed) {
  setTimeout(() => {
    fs.readFile(pathFile, "utf8", (errorGot, data) => {
      if (errorGot) {
        console.error("Error:");
        console.error(errorGot);
      } else {
        let dataGot = JSON.parse(data);
        let infoGot = dataGot.find((eachData) => {
          return eachData.id === idPassed;
        });
        if (infoGot === undefined) {
          let newError = new Error("Error Got Here");
          callback1idPassed(newError, null);
        } else {
          callback1idPassed(null, infoGot);
        }
      }
    });
  }, 2 * 1000);
}

module.exports = callback1;
